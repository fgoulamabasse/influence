import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import { DataContext } from '../dataContext'
import { 
    Icon,
    Modal, 
    Button, 
    Form, 
    FormGroup, 
    ControlLabel, 
    FormControl, 
    Input,
    CheckPicker,
    Divider,
    Notification,
    AutoComplete,
    HelpBlock,
    InputNumber
} from 'rsuite'

type Props = {
    show: boolean
    setShow: Function
    mode: 'edit' | 'add'
    data: any
}

export default function addBooking({ show, setShow, mode, data }) {

    const { data: dataContext, setData: setDataContext } = useContext(DataContext)


    const [saving, setSaving] = useState(false);
    const [selectedPlatforms, setSelectedPlatforms] = useState([])
    const [selectedCampaign, setSelectedCampaign] = useState('');
    const [username, setUsername] = useState('');
    const [cost, setCost] = useState(0);
    const [postsByPlatform, setPostsByPlatform] = useState({})

    useEffect(() => {
        if (mode === 'add') {
            setSelectedPlatforms([])
            setSelectedCampaign('')
            setUsername('')
            setPostsByPlatform({})
        }
    }, [show])

    useEffect(() => {
        if (typeof(data) !== 'undefined' && data.length) {
            const details = dataContext.influencerDetails.filter(item => data[0].booking.influencer && item.influencer.id === data[0].booking.influencer.id);
            const detail = details.find(item => item.platform.id === data[0].booking.influencer.mainPlatform)
            const username = typeof(detail) !== 'undefined' ? detail.username : ''
            const types = {}
            data.forEach((item) => {
                if (types.hasOwnProperty(item.platform.id)) {
                    types[item.platform.id][item.type.id] = item.quantity
                }
                else {
                    types[item.platform.id] = {}
                    types[item.platform.id][item.type.id] = item.quantity
                }
            })
            setSelectedCampaign(data[0].booking.campaign.label)
            setUsername(username)
            setSelectedPlatforms(data.map(item => item.platform.id))
            setCost(data[0].booking.cost)
            setPostsByPlatform(types)
        }
    }, [data])

    useEffect(() => {
        setPostsByPlatform((prev) => {
            let ret = {...prev}
            for (const platform of selectedPlatforms) {
                for (const type of dataContext.postTypes.filter(item => item.platform.id === platform)) {
                    if (ret.hasOwnProperty(platform) === false) {
                        ret[platform] = {}
                        ret[platform][type.id] = 0
                    }        
                }
            }
            return ret
        })
    }, [selectedPlatforms])

    const checkErrors = (influencer, campaign) => {
        if (typeof(influencer) === 'undefined') {
            Notification['error']({
                title: 'Influencer not found'
            })
            return true;
        }
        if (typeof(campaign) === 'undefined') {
            Notification['error']({
                title: 'Campaign not found'
            })
            return true;
        }
        return false;
    }
    
    const addBooking = async () => {
        setSaving(true)
        const influencerDetail = dataContext.influencerDetails.filter(item => item.influencer.country === dataContext.businessUnit.country.id).find(item => item.username === username);
        const campaign = dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).find(item => item.label === selectedCampaign);
        if (checkErrors(influencerDetail, campaign)) {
            setSaving(false)
            return ;
        }
        const bookingData = {
            influencer: influencerDetail.influencer.id,
            campaign: campaign.id,
            cost: cost,
            businessUnit: dataContext.businessUnit.id
        }
        const platformsData = [];
        for (const platformKey of Object.keys(postsByPlatform)) {
            for (const postKey of Object.keys(postsByPlatform[platformKey])) {
                platformsData.push({
                    platform: platformKey,
                    type: postKey,
                    quantity: postsByPlatform[platformKey][postKey]
                })
            }
        }
        const { data: booking, error: errorA } = await supabase.from('bookings').insert(bookingData).single()
        if (booking === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            console.log(errorA)
            setSaving(false);
            return ;
        }
        const { data: details, error: errorB } = await supabase.from('bookingDetails').insert(platformsData.map((item) => { return { booking: booking.id, ...item } }))
        if (details === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            console.log(errorB)
            setSaving(false);
            return ;
        }
        Notification['success']({
            title: 'Booking added !'
        })
        setSaving(false)
        setShow(false)
        const influencer = dataContext.influencers.find(item => item.id === booking.influencer);
        influencer.mainPlatform = influencer.mainPlatform.id
        booking.influencer = influencer
        for (const detail of details) {
            const platform = dataContext.platforms.find(item => item.id === detail.platform);
            const type = dataContext.postTypes.find(item => item.postType.id === detail.type)
            detail.platform = platform
            detail.booking = {...booking}
            detail.booking.campaign = campaign
            detail.type = type.postType
        }
        setDataContext((prev) => {
            return {
                ...prev,
                bookings: [{...booking}, ...prev.bookings],
                bookingDetails: [...details, ...prev.bookingDetails]
            }
        })
    }

    const editBooking = async () => {
        setSaving(true)
        const influencerDetail = dataContext.influencerDetails.filter(item => item.influencer.country === dataContext.businessUnit.country.id).find(item => item.username === username);
        const campaign = dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).find(item => item.label === selectedCampaign);
        if (checkErrors(influencerDetail, campaign)) {
            setSaving(false)
            return ;
        }
        const bookingData = {
            influencer: influencerDetail.influencer.id,
            campaign: campaign.id,
            cost: cost,
            businessUnit: dataContext.businessUnit.id
        }
        const platformsData = [];
        for (const platformKey of Object.keys(postsByPlatform)) {
            for (const postKey of Object.keys(postsByPlatform[platformKey])) {
                platformsData.push({
                    platform: platformKey,
                    type: postKey,
                    quantity: postsByPlatform[platformKey][postKey]
                })
            }
        }
        const { data: booking, error: errorA } = await supabase.from('bookings').update(bookingData).eq('id', data[0].booking.id).single()
        if (booking === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            setSaving(false);
            return ;
        }
        for (const details of platformsData) {
            const { data: insertDetails, error: errorB } = await supabase.from('bookingDetails').update(details).match({ booking: booking.id, platform: details.platform, type: details.type}).single()
            if (insertDetails === null) {
                Notification['error']({
                    title: 'Something went wrong'
                })
                setSaving(false);
                return ;
            }    
        }
        Notification['success']({
            title: 'Booking updated !'
        })
        setSaving(false)
        setShow(false)
    }

    const setPostValue = (platform, type, value) => {
        setPostsByPlatform((prev) => {
            let ret = {...prev}
            if (ret.hasOwnProperty(platform)) {
                ret[platform][type] = value
            }
            else {
                ret[platform] = {}
                ret[platform][type] = value
            }
            return ret
        })
    }

    return (
        <Modal overflow={false} show={show} onHide={() => setShow(false)}>
            <Modal.Header>
                <Modal.Title>{ mode === "add" ? 'Add booking' : 'Edit booking' }</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormGroup>
                        <ControlLabel>Platforms</ControlLabel>
                        <CheckPicker searchable={false} data={dataContext.platforms.map((item) => { return { label: item.name, value: item.id } })} value={selectedPlatforms} onChange={(value) => setSelectedPlatforms(value)} block />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Username</ControlLabel>
                        <AutoComplete
                            data={dataContext.influencerDetails.filter(item => item.influencer.country === dataContext.businessUnit.country.id && (selectedPlatforms.length === 0 || selectedPlatforms.includes(item.platform.id))).map(item => item.username)} value={username} 
                            onChange={(value) => setUsername(value)}
                        ></AutoComplete>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Campaign</ControlLabel>
                        <AutoComplete data={dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).map(item => item.label)} value={selectedCampaign} onChange={(value) => setSelectedCampaign(value)}></AutoComplete>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Cost</ControlLabel>
                        <InputNumber defaultValue="0" step={0.1} value={cost} onChange={(value) => setCost(value as number)}/>
                        <HelpBlock>{dataContext.businessUnit.country.currency}</HelpBlock>
                    </FormGroup>
                    {dataContext.platforms.map((platform) => (
                        selectedPlatforms.includes(platform.id) ?
                        <div key={platform.id}>
                            <Divider>{platform.name}</Divider>
                                {dataContext.postTypes.filter(item => item.platform.id === platform.id).map((postType) => (
                                    <FormGroup key={postType.id}>
                                        <ControlLabel>{postType.postType.name}</ControlLabel>
                                        <InputNumber defaultValue="0" step={1} min={0} value={typeof(postsByPlatform[platform.id]) !== 'undefined' ? postsByPlatform[platform.id][postType.postType.id] : null} onChange={(value) => setPostValue(platform.id, postType.postType.id, value)}/>
                                    </FormGroup>
                                ))}
                        </div>
                        : null
                    ))}
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => mode === 'add' ? addBooking() : editBooking()} appearance="primary">
                    {saving ? <Icon icon="spinner" spin /> : 'Save'}
                </Button>
                <Button onClick={() => setShow(false)} appearance="subtle">
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}