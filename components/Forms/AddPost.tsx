import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import { 
    Icon,
    Modal, 
    Button, 
    Form, 
    FormGroup, 
    ControlLabel, 
    FormControl, 
    Notification,
    AutoComplete,
    InputNumber,
    DatePicker,
} from 'rsuite'
import { DataContext } from '../dataContext'

type Props = {
    show: boolean
    setShow: Function
    mode: 'edit' | 'add'
    data: any
}

export default function addPost({ show, setShow, mode, data }) {

    const { data: dataContext, setData: setDataContext } = useContext(DataContext)

    const [saving, setSaving] = useState(false);
    const [bookingDetails, setBookingDetails] = useState([])
    const [username, setUsername] = useState('');
    const [selectedCampaign, setSelectedCampaign] = useState('')
    const [selectedPlatform, setSelectedPlatform] = useState('')
    const [date, setDate] = useState<Date>(null)
    const [clicks, setClicks] = useState(0)
    const [avgPrints, setAvgPrints] = useState(0)

    useEffect(() => {
        (async function() {
            const { data, error } = await supabase.from('bookingDetails').select('*, booking(id, influencer(id), campaign(id, label)), platform(id, name), type(id, name)')
            setBookingDetails(data)
        })()
    }, [])

    useEffect(() => {
        if (mode === 'add') {
            setUsername('')
            setSelectedCampaign('')
            setSelectedPlatform('')
            setClicks(0)
            setAvgPrints(0)
        }
    }, [show])

    useEffect(() => {
        if (data) {
            const influencer = dataContext.influencerDetails.find(item => data.booking && item.influencer.id === data.booking.influencer.id)
            if (typeof(influencer) !== 'undefined') {
                setSelectedCampaign(data.booking.campaign.label)
                setUsername(influencer.username)
            }
            setSelectedPlatform(data.platform.name)
            setClicks(data.clicks)
            setAvgPrints(data.avgPrints)
            setDate(data.date)
        }
    }, [data])

    const checkErrors = (influencerDetail, campaign, platform, bookingDetail) => {
        if (username.length < 1 || selectedPlatform.length < 1 || selectedCampaign.length < 1) {
            Notification['error']({
                title: 'Some fields are empty'
            })
            return true;
        }
        if (typeof(influencerDetail) === 'undefined') {
            Notification['error']({
                title: 'Influencer not found'
            })
            return true;
        }
        if (typeof(campaign) === 'undefined') {
            Notification['error']({
                title: 'Campaign not found'
            })
            return true;
        }
        if (typeof(platform) === 'undefined') {
            Notification['error']({
                title: 'Platform not found'
            })
            return true;
        }
        if (typeof(bookingDetail) === 'undefined') {
            Notification['error']({
                title: 'Corresponding booking not found'
            })
            return true;
        }

        return false;
    }
    
    const addPost = async () => {
        setSaving(true)
        const influencerDetail = dataContext.influencerDetails.filter(item => item.influencer.country === dataContext.businessUnit.country.id).find(item => item.username === username);
        const campaign = dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).find(item => item.label === selectedCampaign);
        const platform = dataContext.platforms.find(item => item.name === selectedPlatform);
        const bookingDetail = bookingDetails.find(item => 
            influencerDetail && campaign && platform 
            && item.booking && item.booking.influencer && item.booking.campaign
            && item.booking.influencer.id === influencerDetail.influencer.id 
            && item.booking.campaign.id === campaign.id 
            && item.platform.id === platform.id)
        if (checkErrors(influencerDetail, campaign, platform, bookingDetail)) {
            setSaving(false)
            return ;
        }
        const { data: post, error: errorA } = await supabase.from('posts').insert({
            booking: bookingDetail.booking.id,
            platform: platform.id,
            date: date,
            clicks: clicks,
            avgPrints: avgPrints,
        }).single()
        if (errorA) {
            console.log(errorA)
            Notification['error']({
                title: errorA.message
            })
            setSaving(false);
            return ;
        }
        Notification['success']({
            title: 'Post added !'
        })
        setSaving(false)
        setShow(false)
        post.booking = bookingDetail.booking
        setDataContext((prev) => {
            return {
                ...prev,
                posts: [{...post}, ...prev.posts]
            }
        })
    }

    const editPost = async () => {
        setSaving(true)
        const influencer = dataContext.influencers.filter(item => item.country === dataContext.businessUnit.country.id).find(item => item.username === username);
        const campaign = dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).find(item => item.label === selectedCampaign);
        const platform = dataContext.platforms.find(item => item.name === selectedPlatform);
        const bookingDetail = bookingDetails.find(item => 
            influencer && campaign && platform 
            && item.booking && item.booking.influencer && item.booking.campaign
            && item.booking.influencer.id === influencer.id 
            && item.booking.campaign.id === campaign.id 
            && item.platform.id === platform.id)
        if (checkErrors(influencer, campaign, platform, bookingDetail)) {
            setSaving(false)
            return ;
        }
        const { data: post, error: errorA } = await supabase.from('posts').update({
            booking: bookingDetail.booking.id,
            platform: platform.id,
            date: date,
            clicks: clicks,
            avgPrints: avgPrints,
        }).eq('id', data.id).single()
        if (errorA) {
            console.log(errorA)
            Notification['error']({
                title: errorA.message
            })
            setSaving(false);
            return ;
        }
        Notification['success']({
            title: 'Post updated !'
        })
        setSaving(false)
        setShow(false)
    }

    return (
        <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header>
                <Modal.Title>{ mode === "add" ? 'Add post' : 'Edit post' }</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormGroup>
                        <ControlLabel>Username</ControlLabel>
                        <AutoComplete data={dataContext.influencerDetails.filter(item => item.influencer.country === dataContext.businessUnit.country.id).map(item => item.username)} value={username} onChange={(value) => setUsername(value)}></AutoComplete>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Date</ControlLabel>
                        <DatePicker format="YYYY-MM-DD HH:mm:ss" ranges={[{ label: 'Now', value: new Date() } ]} value={date} onChange={(value) => setDate(value)} />
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Campaign</ControlLabel>
                        <AutoComplete data={dataContext.campaigns.filter(item => item.businessUnit === dataContext.businessUnit.id).map(item => item.label)} value={selectedCampaign} onChange={(value) => setSelectedCampaign(value)}></AutoComplete>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Platform</ControlLabel>
                        <AutoComplete data={dataContext.platforms.map(item => item.name)} value={selectedPlatform} onChange={(value) => setSelectedPlatform(value)}></AutoComplete>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Clicks</ControlLabel>
                        <InputNumber value={clicks} min={0} onChange={(value) => setClicks(value as number)}></InputNumber>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Avg Prints</ControlLabel>
                        <InputNumber value={avgPrints} min={0} onChange={(value) => setAvgPrints(value as number)}></InputNumber>
                    </FormGroup>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => mode === 'add' ? addPost() : editPost()} appearance="primary">
                    {saving ? <Icon icon="spinner" spin /> : 'Save'}
                </Button>
                <Button onClick={() => setShow(false)} appearance="subtle">
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}