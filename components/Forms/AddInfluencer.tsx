import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import { 
    Icon,
    Modal, 
    Button, 
    Form, 
    FormGroup, 
    ControlLabel, 
    FormControl, 
    Input,
    CheckPicker,
    Divider,
    Radio,
    RadioGroup,
    Notification,
    HelpBlock
} from 'rsuite'
import { DataContext } from '../dataContext'

type Props = {
    show: boolean
    setShow: Function
    mode: "add" | "edit"
    data?: any[]
}

export default function AddInfluencer({ show, setShow, mode, data }: Props) {

    const { data: dataContext, setData: setDataContext } = useContext(DataContext)

    const [selectedPlatforms, setSelectedPlatforms] = useState([])
    const [selectedMainPlatform, setSelectedMainPlatform] = useState(null)
    const [usernameByPlatform, setUsernameByPlatform] = useState({})
    const [saving, setSaving] = useState(false);

    useEffect(() => {
        if (mode === 'add') {
            setSelectedPlatforms([])
            setSelectedMainPlatform(null)
            setUsernameByPlatform({})
        }
    }, [show])

    useEffect(() => {
        if (typeof(data) !== 'undefined' && data.length) {
            const usernames = {}
            data.forEach((item) => {
                usernames[item.platform.id] = item.username
            })
            setSelectedMainPlatform(data[0].influencer.mainPlatform)
            setSelectedPlatforms(data.map(item => item.platform.id))
            setUsernameByPlatform(usernames)
        }
    }, [data])

    useEffect(() => {
        if (selectedPlatforms.length > 0 && (selectedMainPlatform === null || selectedPlatforms.length === 1)) {
            setSelectedMainPlatform(selectedPlatforms[0])
        }
    }, [selectedPlatforms])

    const checkErrors = () => {
        if (!selectedMainPlatform || selectedPlatforms.length < 1) {
            Notification['error']({
                title: 'Some fields are empty'
            })
            return true;
        }
        if (selectedPlatforms.includes(selectedMainPlatform) === false) {
            Notification['error']({
                title: 'Platforms must contain main platform'
            })
            return true;
        }
        for (const platform of selectedPlatforms) {
            if (usernameByPlatform.hasOwnProperty(platform) === false || usernameByPlatform[platform].length < 1) {
                Notification['error']({
                    title: 'Some fields are empty'
                })
                return true;
            }
            if (mode === 'edit') {
                const checkIfExists = (item) => item.influencer.id !== data[0].influencer.id && item.platform.id === parseInt(platform) && item.username === usernameByPlatform[platform]
                if (dataContext.influencerDetails.some(checkIfExists)) {
                    Notification['error']({
                        title: 'Username already exists'
                    })
                    return true;
                }    
            }
            else {
                const checkIfExists = (item) => item.platform.id === parseInt(platform) && item.username === usernameByPlatform[platform]
                if (dataContext.influencerDetails.some(checkIfExists)) {
                    Notification['error']({
                        title: 'Username already exists'
                    })
                    return true;
                }  
            }
        }
        return false
    }

    const addInfluencer = async () => {
        setSaving(true)
        if (checkErrors()) {
            setSaving(false)
            return ;
        }
        const influencerData = {
            country: dataContext.businessUnit.country.id,
            mainPlatform: selectedMainPlatform,
        }
        const platformsData = [];
        for (const key of Object.keys(usernameByPlatform)) {
            const username = usernameByPlatform[key]
            const platform = dataContext.platforms.find(item => item.id === parseInt(key))
            const info = await fetch('/api/info?platform=' + platform.name + '&username=' + username);
            const result = await info.json();
            if (result.hasOwnProperty('followers') === false) {
                Notification['error']({
                    title: platform.name + ' profile not found'
                })
                setSaving(false)
                return ;
            }
            else {
                const checkIfExists = (item) => item.platform.id === platform.id && item.username === result.username
                if (dataContext.influencerDetails.some(checkIfExists)) {
                    Notification['error']({
                        title: 'Username already exists'
                    })
                    setSaving(false)
                    return ;
                }
                platformsData.push({
                    platform: parseInt(key),
                    username: result.username,
                    followers: result.followers
                })
            }
        }

        let { data: influencer, error: errorA } = await supabase.from('influencers').insert(influencerData).single()
        if (influencer === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            console.log(errorA)
            setSaving(false)
            return ;
        }
        let { data: details, error: errorB } = await supabase.from('influencerDetails').insert(platformsData.map((item) => { return { influencer: influencer.id, ...item } }))
        if (details === null) {
            console.log(errorB)
            Notification['error']({
                title: 'Something went wrong'
            })
            setSaving(false)
            return ;
        }
        Notification['success']({
            title: 'Influencer added !'
        })
        setSaving(false)
        setShow(false)
        const platform = dataContext.platforms.find(item => item.id === influencer.mainPlatform);
        influencer.mainPlatform = platform;
        for (const detail of details) {
            const platform = dataContext.platforms.find(item => item.id === detail.platform);
            detail.platform = platform
            detail.influencer = influencer
        }
        setDataContext((prev) => {
            return {
                ...prev,
                influencers: [{...influencer}, ...prev.influencers],
                influencerDetails: [...details, ...prev.influencerDetails]
            }
        })
    }

    const editInfluencer = async () => {
        setSaving(true)
        if (checkErrors()) {
            setSaving(false)
            return ;
        }
        const influencerData = {
            country: dataContext.businessUnit.country.id,
            mainPlatform: selectedMainPlatform,
        }
        const platformsData = [];
        for (const key of Object.keys(usernameByPlatform)) {
            const username = usernameByPlatform[key]
            const platform = dataContext.platforms.find(item => item.id === parseInt(key))
            const checkIfExists = (item) => item.platform.id === platform.id && item.username === username
            if (dataContext.influencerDetails.some(checkIfExists) === false) {
                const info = await fetch('/api/info?platform=' + platform.name + '&username=' + username);
                const result = await info.json();    
                if (result.hasOwnProperty('followers') === false) {
                    Notification['error']({
                        title: platform.name + ' profile not found'
                    })
                    setSaving(false)
                    return ;
                }
                else {
                    platformsData.push({
                        influencer: data[0].influencer.id,
                        platform: parseInt(key),
                        username: result.username,
                        followers: result.followers
                    })
                }    
            }    
        }

        let { data: influencer, error: errorA } = await supabase.from('influencers').update(influencerData).eq('id', data[0].influencer.id).single()
        if (influencer === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            console.log(errorA)
            setSaving(false)
            return ;
        }
        for (const details of platformsData) {
            let { data: select, error: errorSelect } = await supabase.from('influencerDetails').select('*').match({ influencer: influencer.id, platform: details.platform });
            console.log(select)
            if (select.length === 0) {
                let { data: detailsImport, error: errorB } = await supabase.from('influencerDetails').insert(details).single()
                if (detailsImport === null) {
                    Notification['error']({
                        title: 'Something went wrong'
                    })
                    setSaving(false)
                    return ;
                }
            }
            else {
                let { data: detailsImport, error: errorB } = await supabase.from('influencerDetails').update(details).match({ influencer: influencer.id, platform: details.platform }).single()
                if (detailsImport === null) {
                    Notification['error']({
                        title: 'Something went wrong'
                    })
                    setSaving(false)
                    return ;
                }
            }

        }

        Notification['success']({
            title: 'Influencer updated !'
        })
        setSaving(false)
        setShow(false)
    }

    const setUsername = (platform, value) => {
        setUsernameByPlatform((prev) => {
            let ret = {...prev}
            ret[platform] = value
            return ret
        })
    }

    return (
        <Modal show={show} onHide={() => setShow(false)}>
            <Modal.Header>
                <Modal.Title>{ mode === "add" ? 'Add influencer' : 'Edit influencer' }</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormGroup>
                        <ControlLabel>Platforms</ControlLabel>
                        <CheckPicker searchable={false} data={dataContext.platforms.map((item) => { return { label: item.name, value: item.id } })} value={selectedPlatforms} onChange={(value) => setSelectedPlatforms(value)} block/>
                    </FormGroup>
                    <FormGroup>
                        <ControlLabel>Main platform</ControlLabel>
                        <RadioGroup name="mainPlatform" inline value={selectedMainPlatform} onChange={(value) => setSelectedMainPlatform(value)}>
                            {dataContext.platforms.map((item) => (
                                <Radio key={item.id} value={item.id}>{item.name}</Radio>
                            ))}
                        </RadioGroup>
                    </FormGroup>
                    {dataContext.platforms.map((platform) => (
                        selectedPlatforms.includes(platform.id) ?
                            <div key={platform.id}>
                                <Divider>{platform.name}</Divider>
                                <FormGroup>
                                    <ControlLabel>Username</ControlLabel>
                                    <Input value={usernameByPlatform[platform.id]} onChange={(value) => setUsername(platform.id, value)}/>
                                    <HelpBlock>Username or channel Id</HelpBlock>
                                </FormGroup>
                            </div>
                            : null
                    ))}
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => mode === 'add' ? addInfluencer() : editInfluencer()} appearance="primary">
                    {saving ? <Icon icon="spinner" spin /> : 'Save'}
                </Button>
                <Button onClick={() => setShow(false)} appearance="subtle">
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}
