import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import { DataContext } from '../dataContext'
import { 
    Icon,
    Modal, 
    Button, 
    Form, 
    FormGroup, 
    ControlLabel, 
    FormControl, 
    Input,
    CheckPicker,
    Divider,
    Notification,
    AutoComplete,
    HelpBlock,
    InputNumber
} from 'rsuite'

type Props = {
    show: boolean
    setShow: Function
}

export default function AddCampaign({ show, setShow }) {

    const { data: dataContext, setData: setDataContext } = useContext(DataContext)

    const [saving, setSaving] = useState(false);
    const [selectedCampaign, setSelectedCampaign] = useState('');

    useEffect(() => {
        let suggested = ''
        if (dataContext && dataContext.businessUnit) {
            suggested = dataContext.businessUnit.brand.name.toUpperCase() + '-' + dataContext.businessUnit.country.id + '-IN'
        }  
        setSelectedCampaign(suggested)  
    }, [show])

    useEffect(() => {
        let suggested = ''
        if (dataContext && dataContext.businessUnit) {
            suggested = dataContext.businessUnit.brand.name.toUpperCase() + '-' + dataContext.businessUnit.country.id + '-IN'
        }  
        setSelectedCampaign(suggested)  
    }, [dataContext])

    const checkErrors = () => {
        if (dataContext.campaigns.findIndex(item => item.label === selectedCampaign) !== -1) {
            Notification['error']({
                title: 'Campaign name already exists'
            })
            return true;
        }
        return false;
    }
    
    const addCampaign = async () => {
        setSaving(true)
        if (checkErrors()) {
            setSaving(false)
            return ;
        }
        const { data: campaign, error: errorA } = await supabase.from('campaigns').insert({ label: selectedCampaign, businessUnit: dataContext.businessUnit.id }).single()
        if (campaign === null) {
            Notification['error']({
                title: 'Something went wrong'
            })
            setSaving(false);
            return ;
        }
        Notification['success']({
            title: 'Campaign added !'
        })
        setSaving(false)
        setShow(false)
        setDataContext((prev) => {
            return {
                ...prev,
                campaigns: [{...campaign}, ...prev.campaigns]
            }
        })
    }

    return (
        <Modal overflow={false} show={show} onHide={() => setShow(false)}>
            <Modal.Header>
                <Modal.Title>Add campaign</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form>
                    <FormGroup>
                        <ControlLabel>Campaign name</ControlLabel>
                        <Input value={selectedCampaign} onChange={(value) => setSelectedCampaign(value)} />
                    </FormGroup>
                </Form>
            </Modal.Body>
            <Modal.Footer>
                <Button onClick={() => addCampaign()} appearance="primary">
                    {saving ? <Icon icon="spinner" spin /> : 'Save'}
                </Button>
                <Button onClick={() => setShow(false)} appearance="subtle">
                    Cancel
                </Button>
            </Modal.Footer>
        </Modal>
    )
}