import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import AddInfluencer from '../Forms/AddInfluencer'
import { 
  Icon,
  Button,
  Table,
  IconButton,
  ButtonToolbar,
  Notification,
  Modal,
  Input,
  InputGroup,
  CheckboxGroup,
  Checkbox,
  Form,
  FormGroup,
  ControlLabel,
  RadioGroup,
  Radio,
  Col,
  Row,
  Panel
} from 'rsuite'
import { DataContext } from '../dataContext'

type Details = {
  username: string
  url: string
  followers: number
  engagement: number
  coupon: string
  utm_link: string
}

const { Column, HeaderCell, Cell } = Table;

const UsernameCell = ({ influencerDetails, rowData, dataKey, ...props }) => {
  if (influencerDetails) {
    const details = influencerDetails.filter(item => item.influencer.id === rowData.id);
    const detail = details.find(item => item.platform.id === rowData.mainPlatform.id)
    const username = typeof(detail) !== 'undefined' ? detail.username : null
    return (
      <Cell {...props}>
        {username}
      </Cell>
    )  
  }
  return null;
}

const FollowersCell = ({ influencerDetails, rowData, dataKey, ...props }) => {
  if (influencerDetails) {
    const details = influencerDetails.filter(item => item.influencer.id === rowData.id);
    const detail = details.find(item => item.platform.id === rowData.mainPlatform.id)
    const followers = typeof(detail) !== 'undefined' ? detail.followers : null
    return (
      <Cell {...props}>
        {followers}
      </Cell>
    )  
  }
  return null;
}

const PlatformCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      {rowData[dataKey].name}
    </Cell>
  )
}

const ActionCell = ({ openForm, rowData, dataKey, ...props }) => {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  async function deleteInfluencer() {
    const { data, error } = await supabase.from('influencers').delete().eq('id', rowData[dataKey])
    if (error) {
      Notification['error']({
        title: 'Error',
        description: error.message
      });
    }
    else {
      Notification['success']({
        title: 'Influencer deleted !',
      });
      setDataContext((prev) => {
        let index = prev.influencers.findIndex(item => item.id === rowData[dataKey]);
        let cpy = [...prev.influencers]
        cpy.splice(index, 1)
        return {
          ...prev,
          influencers: cpy
        }
      })
    }
    setShowDeleteModal(false)
  }

  const data = dataContext.influencerDetails ? dataContext.influencerDetails.filter(item => item.influencer.id === rowData.id) : null

  return (
    <>
      <Cell {...props} className="link-group">
        <ButtonToolbar className="flex justify-end">
          <IconButton
            appearance="subtle"
            onClick={() => openForm("edit", data)}
            color="green"
            className="mr-5"
            icon={<Icon icon="edit2" />}
          /> 
          <IconButton
            appearance="subtle"
            onClick={() => setShowDeleteModal(true)}
            color="red"
            icon={<Icon icon="trash" />}
          />
        </ButtonToolbar>
      </Cell>

      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header>
          <Modal.Title>Warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure ?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => deleteInfluencer()} appearance="primary">
              Yes
          </Button>
          <Button onClick={() => setShowDeleteModal(false)} appearance="subtle">
              Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

const PlatformsCell = ({ influencerDetails, rowData, dataKey, ...props }) => {
  const data = influencerDetails ? influencerDetails.filter(item => item.influencer.id === rowData.id) : null
  const [details, setDetails] = useState<Details>({
    username: null,
    url: null,
    followers: null,
    engagement: null,
    coupon: null,
    utm_link: null,
  });
  const [showDetail, setShowDetail] = useState(false);

  function pickDetails(data: Details) {
    setDetails(data);
    setShowDetail(true);
  }

  if (influencerDetails) {  
    return (
      <>
      <Cell {...props} className="">
        <ButtonToolbar className="flex center">
          {data.map(item => (
            <IconButton 
              key={item.id}
              icon={<Icon icon={item.platform.name} />}
              className="mx-2"
              onClick={() => pickDetails(item)}
              circle />
          ))}
        </ButtonToolbar>
      </Cell>
  
      <Modal show={showDetail} onHide={() => setShowDetail(false)}>
          <Modal.Header>
            <div className="text-lg font-bold text-black text-center w-full">{details.username}'s info</div>
          </Modal.Header>
          <Modal.Body>
            <Row className="p-2">
              <Col lg={12} className="p-2">
                <Panel {...props} bordered header={<span className="text-xl">UTM link</span>}>
                  <span className="text-lg">{details.utm_link}</span>
                </Panel>
              </Col>
              <Col lg={12} className="p-2">
                <Panel {...props} bordered header={<span className="text-xl">Coupon</span>}>
                  <span className="text-lg">{details.coupon}</span>
                </Panel> 
              </Col>
            </Row>
            <Row className="p-2">
            <Col lg={12} className="p-2">
                <Panel {...props} bordered header={<span className="text-xl">Engagement rate</span>}>
                  <span className="text-lg">{details.engagement && details.engagement.toFixed(2)}%</span>
                </Panel>
              </Col>
              <Col lg={12} className="p-2">
                <Panel {...props} bordered header={<span className="text-xl">Followers</span>}>
                  <span className="text-lg">{details.followers}</span>
                </Panel>              
              </Col>
            </Row>
            <Panel {...props} bordered header={<span className="text-xl">Link</span>}>
              <a className="text-lg" href={details.url} target="_blank" rel="noopener noreferrer">{details.url}</a>
            </Panel>
          </Modal.Body>
          <Modal.Footer>
              <Button onClick={() => setShowDetail(false)} appearance="subtle">
                  Cancel
              </Button>
          </Modal.Footer>
        </Modal>
        </>
    )
  }
  return null;
}

export default function InfluencersList({ }) {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [list, setList] = useState(null);
  const [errorText, setError] = useState('');
  const [selectedPlatforms, setSelectedPlatforms] = useState(dataContext.platforms.map(item => item.id));
  const [selectedMainPlatform, setSelectedMainPlatform] = useState(null);
  const [filteredItems, setFilteredItems] = useState(null);
  const [textFilter, setTextFilter] = useState('');
  const [showForm, setShowForm] = useState(false);
  const [formMode, setFormMode] = useState<"add" | "edit">("add");
  const [editData, setEditData] = useState([]);
  const [sortType, setSortType] = useState<'desc' | 'asc'>('asc');
  const [tableLoading, setTableLoading] = useState(false);


  useEffect(() => {
    setList(dataContext.influencers.filter(item => item.country === dataContext.businessUnit.country.id))
    setFilteredItems(dataContext.influencers.filter(item => item.country === dataContext.businessUnit.country.id))
  }, [dataContext])

  useEffect(() => {
    if (list) {
      const infFilter = dataContext.influencerDetails.filter(item => item.username && item.username.startsWith(textFilter) && selectedPlatforms.includes(item.platform.id));
      const result = list.filter(item => infFilter.some(inf => inf.influencer.id === item.id) && (selectedMainPlatform === null || item.mainPlatform.id === selectedMainPlatform))
      setFilteredItems(result);
    }
  }, [textFilter, selectedMainPlatform, selectedPlatforms])

  const openForm = function(mode, data?) {
    setShowForm(true);
    setFormMode(mode);
    setEditData(data);
  }

  const sortColumn = function(col, type) {
    setTableLoading(true)
    setFilteredItems((prev) => {
      const copy = [...prev];
      copy.sort((a, b) => {
        let x = a[col]
        let y = b[col]
        if (col === 'username') {
          const detailsA = dataContext.influencerDetails.filter(item => item.influencer.id === a.id);
          const detailA = detailsA.find(item => item.platform.id === a.mainPlatform.id)
          x = typeof(detailA) !== 'undefined' ? detailA.username : ''      
          const detailsB = dataContext.influencerDetails.filter(item => item.influencer.id === b.id);
          const detailB = detailsB.find(item => item.platform.id === b.mainPlatform.id)
          y = typeof(detailB) !== 'undefined' ? detailB.username : ''
        }
        if (col === 'followers') {
          const detailsA = dataContext.influencerDetails.filter(item => item.influencer.id === a.id);
          const detailA = detailsA.find(item => item.platform.id === a.mainPlatform.id)
          x = typeof(detailA) !== 'undefined' ? detailA.followers : 0
          const detailsB = dataContext.influencerDetails.filter(item => item.influencer.id === b.id);
          const detailB = detailsB.find(item => item.platform.id === b.mainPlatform.id)
          y = typeof(detailB) !== 'undefined' ? detailB.followers : 0      
        }
        if (typeof x === 'string' && typeof y === 'string') {
          return type === 'asc' ? x.localeCompare(y) : y.localeCompare(x)
        }
        return type === 'asc' ? x - y : y - x
      })
      return copy
    })
    setSortType(prev => prev === 'asc' ? 'desc' : 'asc')
    setTableLoading(false);
  }

  if (filteredItems === null) {
    return (
      <div className="w-full h-screen flex justify-center items-center">
        <Icon icon="spinner" size="4x" spin className=""/>
      </div>
    )
  }

  return (
    <div className="w-full">
      <h1 className="mb-12">
        {filteredItems.length.toString() + " Influencers."}
      </h1>

      <div className="my-5 flex w-full justify-center">
        <div className="w-1/2">
          <InputGroup size="lg" inside>
            <Input placeholder="Search by username..." onChange={(value) => setTextFilter(value)} value={textFilter}/>
              <InputGroup.Button>
                <Icon icon="search" />
              </InputGroup.Button>
          </InputGroup>
        </div>
        <Button onClick={() => openForm("add")} className="ml-5 w-40" appearance="primary">Add influencer</Button>
      </div>

        <Form className="m-10 p-5 bg-white">
          <FormGroup>
            <ControlLabel>MainPlatform</ControlLabel>
            <RadioGroup
              inline
              name="radioList"
              value={selectedMainPlatform}
              onChange={(value) => setSelectedMainPlatform(value)}
            >
              {dataContext.platforms.map(item => <Radio key={item.id} value={item.id}>{item.name}</Radio>)}
            </RadioGroup>
          </FormGroup>
          <FormGroup>
            <ControlLabel>Platforms</ControlLabel>
            <CheckboxGroup
              inline
              name="checkboxList"
              value={selectedPlatforms}
              onChange={(value) => setSelectedPlatforms(value)}
            >
              {dataContext.platforms.map(item => <Checkbox key={item.id} value={item.id}>{item.name}</Checkbox>)}
            </CheckboxGroup>
          </FormGroup>
        </Form>

      <div>
        <Table
          rowHeight={60}
          data={filteredItems}
          onSortColumn={(col, type) => sortColumn(col, type)}
          autoHeight
          sortType={sortType}
          loading={tableLoading}  
        >
          <Column align="center" sortable>
            <HeaderCell>Id</HeaderCell>
            <Cell dataKey="id" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Username</HeaderCell>
            <UsernameCell influencerDetails={dataContext.influencerDetails} rowData dataKey="username" />
          </Column>

          <Column sortable>
            <HeaderCell>Followers</HeaderCell>
            <FollowersCell influencerDetails={dataContext.influencerDetails} rowData dataKey="followers" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Main platform</HeaderCell>
            <PlatformCell rowData dataKey="mainPlatform" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Platforms</HeaderCell>
            <PlatformsCell influencerDetails={dataContext.influencerDetails} rowData dataKey="platforms" />
          </Column>
          
          <Column flexGrow={1} fixed="right">
            <HeaderCell>Action</HeaderCell>
            <ActionCell openForm={openForm} dataKey={'id'} rowData />
          </Column>
        </Table>
      </div>

      <AddInfluencer show={showForm} setShow={setShowForm} mode={formMode} data={editData} />
    </div>
  )
}