import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import AddBooking from '../Forms/AddBooking'
import { DataContext } from '../dataContext'

import { 
  Icon,
  Button, 
  InputGroup,
  Input,
  Table,
  ButtonToolbar,
  IconButton,
  Whisper,
  Popover,
  Modal,
  Notification
} from 'rsuite'

const { Column, HeaderCell, Cell } = Table;

type Influencer = {
  username: string
  country: string
  id: number
  mainPlatform: number
}

const UsernameCell = ({ influencerDetails, rowData, dataKey, ...props }) => {
  let username = null;
  if (rowData.influencer) {
    const details = influencerDetails.filter(item => item.influencer.id === rowData.influencer.id);
    const data = details.find(item => item.platform.id === rowData.influencer.mainPlatform)
    username = typeof(data) !== 'undefined' ? data.username : null
  }
  return (
    <Cell {...props}>
      {username}
    </Cell>
  )
}

const CampaignCell = ({ campaigns, rowData, dataKey, ...props }) => {
  const campaign = campaigns.find(item => item.id === rowData[dataKey])
  return (
    <Cell {...props}>
      {typeof(campaign) !== 'undefined' && campaign.label}
    </Cell>
  )
}

const PlatformsCell = ({ bookingDetails, rowData, dataKey, ...props }) => {
  const data = bookingDetails.filter(item => item.booking.id === rowData.id)

  return (
    <Cell {...props} className="">
      <ButtonToolbar className="flex center">
        {data.map(item => item.quantity > 0 ? (
          <Whisper key={item.id} trigger="hover" placement="top" speaker={<Popover><p>{item.quantity + " " + item.type.name}</p></Popover>}>
            <IconButton 
              icon={<Icon icon={item.platform.name} />}
              className="mx-2"
              circle 
            />
          </Whisper>
        ) : null)}
      </ButtonToolbar>
    </Cell>
  )
}

const ActionCell = ({ openForm, rowData, dataKey, ...props }) => {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  async function deleteBooking() {
    const { data, error } = await supabase.from('bookings').delete().eq('id', rowData.id)
    if (error) {
      Notification['error']({
        title: 'Error',
        description: error.message
      });
    }
    else {
      Notification['success']({
        title: 'Booking deleted !',
      });
      setDataContext((prev) => {
        let index = prev.bookings.findIndex(item => item.id === rowData.id);
        let cpy = [...prev.bookings]
        cpy.splice(index, 1)
        return {
          ...prev,
          bookings: cpy
        }
      })
    }
    setShowDeleteModal(false)
  }

  const data = dataContext.bookingDetails.filter(item => item.booking.id === rowData.id)

  return (
    <>
      <Cell {...props} className="link-group">
        <ButtonToolbar className="flex justify-end">
          <IconButton
            appearance="subtle"
            onClick={() => openForm("edit", data)}
            color="green"
            className="mr-5"
            icon={<Icon icon="edit2" />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setShowDeleteModal(true)}
            color="red"
            icon={<Icon icon="trash" />}
          />
        </ButtonToolbar>
      </Cell>

      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header>
          <Modal.Title>Warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure ?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => deleteBooking()} appearance="primary">
              Yes
          </Button>
          <Button onClick={() => setShowDeleteModal(false)} appearance="subtle">
              Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default function BookingsList({ }) {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [list, setList] = useState(null)
  const [errorText, setError] = useState('')
  const [filteredItems, setFilteredItems] = useState(null);
  const [textFilter, setTextFilter] = useState('');
  const [bookingDetails, setBookingDetails] = useState([])
  const [showForm, setShowForm] = useState(false);
  const [formMode, setFormMode] = useState<"add" | "edit">("add");
  const [editData, setEditData] = useState([]);
  const [sortType, setSortType] = useState<'desc' | 'asc'>('asc');
  const [tableLoading, setTableLoading] = useState(false);


  useEffect(() => {
    setList(dataContext.bookings.filter(item => item.businessUnit === dataContext.businessUnit.id))
    setFilteredItems(dataContext.bookings.filter(item => item.businessUnit === dataContext.businessUnit.id))
  }, [dataContext])

  useEffect(() => {
    if (list) {
      const infFilter = dataContext.influencerDetails.filter(item => item.username && item.username.includes(textFilter));
      const result = list.filter(item => infFilter.some(inf => item.influencer && inf.influencer.id === item.influencer.id))
      setFilteredItems(result);
    }
  }, [textFilter])

  const openForm = function(mode, data?) {
    setShowForm(true);
    setFormMode(mode);
    setEditData(data);
  }

  const sortColumn = function(col, type) {
    setTableLoading(true)
    setFilteredItems((prev) => {
      const copy = [...prev];
      copy.sort((a, b) => {
        let x = a[col]
        let y = b[col]
        if (col === 'username') {
          const detailsA = dataContext.influencerDetails.filter(item => a.influencer && item.influencer.id === a.influencer.id);
          const detailA = detailsA.find(item => item.platform.id === a.influencer.mainPlatform)
          x = typeof(detailA) !== 'undefined' ? detailA.username : ''
          const detailsB = dataContext.influencerDetails.filter(item => b.influencer && item.influencer.id === b.influencer.id);
          const detailB = detailsB.find(item => item.platform.id === b.influencer.mainPlatform)
          y = typeof(detailB) !== 'undefined' ? detailB.username : ''
        }
        if (typeof x === 'string' && typeof y === 'string') {
          return type === 'asc' ? x.localeCompare(y) : y.localeCompare(x)
        }
        return type === 'asc' ? x - y : y - x
      })
      return copy
    })
    setSortType(prev => prev === 'asc' ? 'desc' : 'asc')
    setTableLoading(false);
  }

  if (filteredItems === null) {
    return (
      <div className="w-full h-screen flex justify-center items-center">
        <Icon icon="spinner" size="4x" spin className=""/>
      </div>
    )
  }

  return (
    <div className="w-full">
      <h1 className="mb-12">{filteredItems.length.toString() + " Bookings."}</h1>
      {!!errorText && <Alert text={errorText} />}

      <div className="my-5 flex w-full justify-center">
        <div className="w-1/2">
          <InputGroup size="lg" inside>
            <Input placeholder="Search by username..." onChange={(value) => setTextFilter(value)} value={textFilter}/>
              <InputGroup.Button>
                <Icon icon="search" />
              </InputGroup.Button>
          </InputGroup>
        </div>
        <Button onClick={() => openForm("add")} className="ml-5 w-40" appearance="primary">Add booking</Button>
      </div>

      <Table
          rowHeight={60}
          data={filteredItems}
          onSortColumn={(col, type) => sortColumn(col, type)}
          autoHeight
          sortType={sortType}
          loading={tableLoading}  
        >
          <Column align="center" sortable>
            <HeaderCell>Id</HeaderCell>
            <Cell dataKey="id" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Influencer</HeaderCell>
            <UsernameCell influencerDetails={dataContext.influencerDetails} rowData dataKey="username" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Campaign</HeaderCell>
            <CampaignCell campaigns={dataContext.campaigns} rowData dataKey="campaign" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>{'Cost (in ' + dataContext.businessUnit.country.currency + ')'}</HeaderCell>
            <Cell dataKey="cost" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Platforms</HeaderCell>
            <PlatformsCell bookingDetails={dataContext.bookingDetails} rowData dataKey="cost" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Action</HeaderCell>
            <ActionCell openForm={openForm} rowData dataKey="id" />
          </Column>
      </Table>

      <AddBooking show={showForm} setShow={setShowForm} mode={formMode} data={editData}/>
      
    </div>
  )
}

const Alert = ({ text }) => (
  <div className="rounded-md bg-red-100 p-4 my-3">
    <div className="text-sm leading-5 text-red-700">{text}</div>
  </div>
)
