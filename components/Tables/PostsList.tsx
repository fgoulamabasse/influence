import { useState, useEffect, useContext } from 'react'
import { supabase } from '../../lib/initSupabase'
import AddPost from '../Forms/AddPost'
import { 
  Icon,
  Button,
  InputGroup,
  Input,
  Table,
  Modal,
  Notification,
  ButtonToolbar,
  IconButton
} from 'rsuite'
import { DataContext } from '../dataContext';
import moment from 'moment'

const { Column, HeaderCell, Cell } = Table;

type Influencer = {
  username: string
  country: string
  id: number
  mainPlatform: number
}

const UsernameCell = ({ influencerDetails, rowData, dataKey, ...props }) => {
  let username = null;
  if (rowData.booking && rowData.booking.influencer) {
    const details = influencerDetails.filter(item => item.influencer.id === rowData.booking.influencer.id);
    const data = details.find(item => item.platform.id === rowData.booking.influencer.mainPlatform)
    username = typeof(data) !== 'undefined' ? data.username : null
  }
  return (
    <Cell {...props}>
      {username}
    </Cell>
  )
}

const CampaignCell = ({ campaigns, rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      {rowData.booking && rowData.booking.campaign.label}
    </Cell>
  )
}

const PlatformCell = ({ rowData, dataKey, ...props }) => {
  return (
    <Cell {...props}>
      <Icon icon={rowData[dataKey].name}></Icon>
    </Cell>
  )
}

const ActionCell = ({ openForm, rowData, dataKey, ...props }) => {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [showDeleteModal, setShowDeleteModal] = useState(false);

  async function deletePost() {
    const { data, error } = await supabase.from('posts').delete().eq('id', rowData.id)
    if (error) {
      Notification['error']({
        title: 'Error',
        description: error.message
      });
    }
    else {
      Notification['success']({
        title: 'Post deleted !',
      });
      setDataContext((prev) => {
        let index = prev.posts.findIndex(item => item.id === rowData.id);
        let cpy = [...prev.posts]
        cpy.splice(index, 1)
        return {
          ...prev,
          posts: cpy
        }
      })
    }
    setShowDeleteModal(false)
  }
  
  return (
    <>
      <Cell {...props} className="link-group">
        <ButtonToolbar className="flex justify-end">
          <IconButton
            appearance="subtle"
            onClick={() => openForm("edit", rowData)}
            color="green"
            className="mr-5"
            icon={<Icon icon="edit2" />}
          />
          <IconButton
            appearance="subtle"
            onClick={() => setShowDeleteModal(true)}
            color="red"
            icon={<Icon icon="trash" />}
          />
        </ButtonToolbar>
      </Cell>

      <Modal show={showDeleteModal} onHide={() => setShowDeleteModal(false)}>
        <Modal.Header>
          <Modal.Title>Warning</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          Are you sure ?
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => deletePost()} appearance="primary">
              Yes
          </Button>
          <Button onClick={() => setShowDeleteModal(false)} appearance="subtle">
              Cancel
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default function PostsList({ }) {

  const { data: dataContext, setData: setDataContext } = useContext(DataContext)

  const [list, setList] = useState(null)
  const [filteredItems, setFilteredItems] = useState(null);
  const [textFilter, setTextFilter] = useState('');
  const [showForm, setShowForm] = useState(false);
  const [formMode, setFormMode] = useState<"add" | "edit">("add");
  const [editData, setEditData] = useState(null);
  const [sortType, setSortType] = useState<'desc' | 'asc'>('asc');
  const [tableLoading, setTableLoading] = useState(false);

  useEffect(() => {
    setList(dataContext.posts.filter(item => item.booking === null || item.booking.businessUnit === dataContext.businessUnit.id))
    setFilteredItems(dataContext.posts.filter(item => item.booking === null || item.booking.businessUnit === dataContext.businessUnit.id))
  }, [dataContext])

  useEffect(() => {
    if (list) {
      const infFilter = dataContext.influencerDetails.filter(item => item.username && item.username.includes(textFilter));
      const result = list.filter(item => infFilter.some(inf => item.booking && item.booking.influencer && inf.influencer.id === item.booking.influencer.id))
      setFilteredItems(result);
    }
  }, [textFilter])

  const openForm = function(mode, data?) {
    setShowForm(true);
    setFormMode(mode);
    setEditData(data);
  }

  const sortColumn = function(col, type) {
    setTableLoading(true)
    setFilteredItems((prev) => {
      const copy = [...prev];
      copy.sort((a, b) => {
        let x = a[col]
        let y = b[col]
        if (col === 'username') {
          const detailsA = dataContext.influencerDetails.filter(item => a.booking && a.booking.influencer && item.influencer.id === a.booking.influencer.id);
          const detailA = detailsA.find(item => item.platform.id === a.booking.influencer.mainPlatform)
          x = typeof(detailA) !== 'undefined' ? detailA.username : ''
          const detailsB = dataContext.influencerDetails.filter(item => b.booking && b.booking.influencer && item.influencer.id === b.booking.influencer.id);
          const detailB = detailsB.find(item => item.platform.id === b.booking.influencer.mainPlatform)
          y = typeof(detailB) !== 'undefined' ? detailB.username : ''
        }
        if (col === 'campaign') {
          x = a.booking ? a.booking.campaign.label : ''
          y = b.booking ? b.booking.campaign.label : ''
        }
        if (col === 'date') {
          if (type === 'asc') {
            return moment(a.date).isBefore(moment(b.date)) ? -1 : 1;

          } else {
            return moment(b.date).isBefore(moment(a.date)) ? -1 : 1;
          }
        }
        if (typeof x === 'string' && typeof y === 'string') {
          return type === 'asc' ? x.localeCompare(y) : y.localeCompare(x)
        }
        return type === 'asc' ? x - y : y - x
      })
      return copy
    })
    setSortType(prev => prev === 'asc' ? 'desc' : 'asc')
    setTableLoading(false);
  }

  if (filteredItems === null) {
    return (
      <div className="w-full h-screen flex justify-center items-center">
        <Icon icon="spinner" size="4x" spin className=""/>
      </div>
    )
  }

  return (
    <div className="w-full">
      <h1 className="mb-12">{filteredItems.length.toString() + " Posts."}</h1>

      <div className="my-5 flex w-full justify-center">
        <div className="w-1/2">
          <InputGroup size="lg" inside>
            <Input placeholder="Search by username..." onChange={(value) => setTextFilter(value)} value={textFilter}/>
              <InputGroup.Button>
                <Icon icon="search" />
              </InputGroup.Button>
          </InputGroup>
        </div>
        <Button onClick={() => openForm('add')} className="ml-5 w-40" appearance="primary">Add post</Button>
      </div>

      <Table
        rowHeight={60}
        data={filteredItems}
        onSortColumn={(col, type) => sortColumn(col, type)}
        autoHeight
        sortType={sortType}
        loading={tableLoading}
      >
          <Column sortable flexGrow={1}>
            <HeaderCell>Date</HeaderCell>
            <Cell dataKey="date" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Influencer</HeaderCell>
            <UsernameCell influencerDetails={dataContext.influencerDetails} rowData dataKey="username" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Campaign</HeaderCell>
            <CampaignCell campaigns={dataContext.campaigns} rowData dataKey="campaign" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Platform</HeaderCell>
            <PlatformCell rowData dataKey="platform" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>Clicks</HeaderCell>
            <Cell dataKey="clicks" />
          </Column>

          <Column sortable flexGrow={1}>
            <HeaderCell>AvgPrints</HeaderCell>
            <Cell dataKey="avgPrints" />
          </Column>

          <Column flexGrow={1}>
            <HeaderCell>Action</HeaderCell>
            <ActionCell openForm={openForm} rowData dataKey="id" />
          </Column>
      </Table>

      <AddPost show={showForm} setShow={setShowForm} mode={formMode} data={editData}/>

    </div>
  )
}