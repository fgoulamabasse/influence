import { createContext } from 'react'

export const BusinessUnitContext = createContext(null);