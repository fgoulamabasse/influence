import { supabase } from './initSupabase'

async function getAll(table, query, ascending = true) {
    let items = [];
    let page = 0;
    while (1) {
        const { data, error } = await supabase.from(table).select(query).range(page * 900, page * 900 + 900).order('id', { ascending: ascending });
        if (error) {
            return null;
        }
        page += 1
        if (items.length) {
            items = [...items, ...data]
        }
        else {
            items = [...data]
        }
        if (data.length < 900) {
            break ;
        }
    }
    return items;
}

export async function getDataFromSupabase() {
    const brands = await getAll('brands', '*');
    const countries = await getAll('countries', '*');
    const platforms = await getAll('platforms', '*');
    const campaigns = await getAll('campaigns', '*', false);
    const postTypes = await getAll('platformPostTypes', '*, platform(id, name), postType(id, name)');
    const bookings = await getAll('bookings', '*, influencer(id, country, mainPlatform)');
    const posts = await getAll('posts', '*, platform(id, name), booking(id, influencer(id, mainPlatform), campaign(id, label), businessUnit)');
    const influencers = await getAll('influencers', '*, mainPlatform(id, name)');
    const influencerDetails = await getAll('influencerDetails', '*, influencer(id, country, mainPlatform), platform(id, name)');
    const bookingDetails = await getAll('bookingDetails', `
        *,
        booking(id, influencer(id, mainPlatform), campaign(id, label), cost, businessUnit),
        platform(id, name),
        type(id, name)
    `);

    if (!brands || !countries || !platforms || !campaigns || !postTypes || !bookings || !posts || !influencers || !influencerDetails || !bookingDetails) {
        return null;
    }

    return {
        brands: brands,
        countries: countries,
        influencers: influencers,
        influencerDetails: influencerDetails,
        bookings: bookings,
        bookingDetails: bookingDetails,
        posts: posts,
        campaigns: campaigns,
        platforms: platforms,
        postTypes: postTypes
    }  
}