import { useState, useEffect } from 'react'
import moment from 'moment'
import InfluencersList from '../components/Tables/InfluencersList'
import BookingsList from '../components/Tables/BookingsList'
import PostsList from '../components/Tables/PostsList'
import { ToastContainer, toast } from 'react-toastify';
import { supabase } from '../lib/initSupabase'
import { getDataFromSupabase } from '../lib/getDataFromSupabase'
import { Sidenav, Nav, Icon, Dropdown, Button } from 'rsuite';
import { DataContext } from '../components/dataContext'
import AddCampaign from '../components/Forms/AddCampaign';

export default function IndexPage() {
  const [main, setMain] = useState(<InfluencersList/>)
  const [active, setActive] = useState('1');
  const [showForm, setShowForm] = useState(false);
  const [loading, setLoading] = useState(true);
  const [selectedCountry, setSelectedCountry] = useState(null);
  const [selectedBrand, setSelectedBrand] = useState(null);
  const [selectedBU, setSelectedBU] = useState(null);
  const [brands, setBrands] = useState([]);
  const [countries, setCountries] = useState([]);
  const [data, setData] = useState({
    businessUnit: null,
    influencers: null,
    influencerDetails: null,
    bookings: null,
    bookingDetails: null,
    posts: null,
    campaigns: null,
    countries: null,
    platforms: null,
    postTypes: null
  });

  useEffect(() => {

    async function findInfluencer(coupon, landing) {
      if (coupon) {
          const { data: influencerDetail, error } = await supabase.from('influencerDetails').select('influencer').eq('coupon', coupon).single()
          return influencerDetail ? influencerDetail.influencer : null
      }
      else {
          let index = landing.search("utm_source")
          if (index === -1) {
              return null;
          }
          else {
              let source = landing.substr(index);
              source = source.substr(source.indexOf('=') + 1);
              const { data: influencerDetail, error } = await supabase.from('influencerDetails').select('influencer').eq('username', source).single()
              return influencerDetail ? influencerDetail.influencer : null
          }
      }
  }
  
    async function findPost(coupon, landing, created_at) {
        const influencerId = findInfluencer(coupon, landing)
        if (influencerId) {
            const { data: posts, error } = await supabase.from('posts').select('id, date, influencer').order('date', { ascending: false }).eq('influencer', influencerId)
            if (posts) {
                const postsBefore = posts.filter(item => item.influencer && item.influencer === influencerId && moment(item.date).isBefore(moment(created_at)));
                const result = postsBefore.length > 0 ? postsBefore[0] : null;
                return result;
            }
        }
        return null;
    }

    (async function() {
      const attributionSub = supabase.from('attribution')
      .on('INSERT', payload => {
        console.log('Change received!', payload)
        toast("New order of " + payload.new.price.toString() + "€ processed!");
      })
      .subscribe()

      const postSub = supabase.from('posts')
      .on('INSERT', async (payload) => {
        console.log('Change received!', payload)
        const post = payload.new;
        const postDate = moment(post.date).format("YYYY-MM-DD HH:mm:ss");
        const { data: ordersInDB, error }  = await supabase.from('attribution').select('*').gt('date', postDate);
        for (const order of ordersInDB) {
          const foundPost = await findPost(order.coupon, order.landing, order.date)
          if (foundPost) {
            const { data, error } = await supabase.from('attribution')
            .update({
              post: foundPost.id
            })
            .match({ id: order.id })
            console.log(data)  
          }
        }
      })
      .subscribe()

      const dataFromDb = await getDataFromSupabase();
      if (!dataFromDb) {
        console.log('something went wrong')
      }

      setBrands(dataFromDb.brands);
      setCountries(dataFromDb.countries);
      setSelectedBrand(1)
      setSelectedCountry('FR')
      const { data: businessUnit, error } = await supabase.from('businessUnit').select('*, brand(id, name), country(id, currency)').match({ brand: 1, country: 'FR' }).single();
      setSelectedBU(businessUnit);
      setData({
        businessUnit: businessUnit,
        influencers: dataFromDb.influencers,
        influencerDetails: dataFromDb.influencerDetails,
        bookings: dataFromDb.bookings,
        bookingDetails: dataFromDb.bookingDetails,
        posts: dataFromDb.posts,
        campaigns: dataFromDb.campaigns,
        countries: dataFromDb.countries,
        platforms: dataFromDb.platforms,
        postTypes: dataFromDb.postTypes
      })

      setLoading(false)

      return function cleanup() {
        supabase.removeSubscription(attributionSub)
        supabase.removeSubscription(postSub)
      }
    })()
  }, [])

  useEffect(() => {
    if (active === '1') {
      setMain(<InfluencersList/>)
    }
    else if (active === '2') {
      setMain(<BookingsList/>)
    }
    else if (active === '3') {
      setMain(<PostsList/>)
    }
  }, [active])

  useEffect(() => {
    (async function() {
      setLoading(true)
      if (!selectedBrand || !selectedCountry) return ;

      const { data: businessUnit, error } = await supabase.from('businessUnit').select('*, brand(id, name), country(id, currency)').match({ brand: selectedBrand, country: selectedCountry }).single();
      setSelectedBU(businessUnit);
      if (businessUnit) {
        setData((prev) => {
          return {
            ...prev,
            businessUnit: businessUnit
          }
        })
      }
      setLoading(false)
    })()
  }, [selectedBrand, selectedCountry])

  return (
    <div className="w-full h-full bg-gray-300">
      <div className="fixed left-0" style={{ width: '250px' }}>
        <Sidenav activeKey={active} onSelect={setActive} className="h-screen py-10 px-5 relative">
          <Sidenav.Header className="w-full mb-8 h-1/12 flex justify-center items-center">
            <span className="font-bold text-xl text-center">INFLUENCE MANAGER</span>
          </Sidenav.Header>
          <Sidenav.Body>
            <Dropdown className="mx-1 my-3" appearance="ghost" eventKey={20} title={brands.length ? brands[0].name : ''} icon={<Icon icon="magic" />}>
              {brands.map((item) => <Dropdown.Item key={item.id} onClick={(e) => setSelectedBrand(brands.find(item => item.name === e.target.innerHTML).id)}>{item.name}</Dropdown.Item>)}
            </Dropdown>
            <Dropdown className="mx-1" appearance="ghost" eventKey={10} title={selectedCountry} icon={<Icon icon="flag" />}>
              {countries.map((item) => <Dropdown.Item
                key={item.id} 
                onClick={(e) => setSelectedCountry(countries.find(item => item.id === e.target.innerHTML).id)}
              >
                  {item.id}
              </Dropdown.Item>)}
            </Dropdown>
            <Nav className="my-3">
              <Nav.Item eventKey="1" icon={<Icon icon="group" />}>
                Influencers
              </Nav.Item>
              <Nav.Item eventKey="2" icon={<Icon icon="credit-card" />}>
                Bookings
              </Nav.Item>
              <Nav.Item eventKey="3" icon={<Icon icon="comment" />}>
                Posts
              </Nav.Item>
            </Nav>
          </Sidenav.Body>
          <div className="flex w-full justify-center pt-5">
            <Button appearance="primary" onClick={() => setShowForm(true)}>
              Add campaign
            </Button>
          </div>
        </Sidenav>
      </div>

      <div className="flex w-full justify-center pt-5" style={{ paddingLeft: '260px' }}>
        <div
          className="w-full h-full flex flex-col justify-center items-center p-4"
        >
          <DataContext.Provider value={{ data, setData }}>
            {loading ? 
                (<div className="w-full h-screen flex justify-center items-center">
                  <Icon icon="spinner" size="4x" spin className=""/>
                </div>)
            : selectedBU ? main : 'Error: no business unit found'}
            <AddCampaign show={showForm} setShow={setShowForm}/>
          </DataContext.Provider>
        </div>
        <ToastContainer autoClose={false} hideProgressBar={true} newestOnTop={true}/>
      </div>
    </div>
  )
}
