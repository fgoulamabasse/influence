import axios from 'axios';
import cheerio from 'cheerio'
import puppeteer from 'puppeteer'
import chrome from 'chrome-aws-lambda'

async function getYoutubeInfo(username) {
    const url = 'https://youtube.googleapis.com/youtube/v3/channels?part=statistics,contentOwnerDetails,contentDetails,brandingSettings&forUsername=' + username
        + '&key=' + process.env.NEXT_PUBLIC_YOUTUBE_API_KEY;
    const response = await axios(url);
    if (typeof(response.data.items) !== 'undefined') {
        return {
            username: response.data.items[0].brandingSettings.channel.title,
            followers: response.data.items[0].statistics.subscriberCount,
            ...response.data.items[0].statistics
        }
    }
    else {
        const url = 'https://youtube.googleapis.com/youtube/v3/channels?part=statistics,contentOwnerDetails,contentDetails,brandingSettings&id=' + username
        + '&key=' + process.env.NEXT_PUBLIC_YOUTUBE_API_KEY;
        const response = await axios(url);
        if (typeof(response.data.items) !== 'undefined') {
            return {
                username: response.data.items[0].brandingSettings.channel.title,
                followers: response.data.items[0].statistics.subscriberCount,
                ...response.data.items[0].statistics
            }
        }
        return {
            username: username
        };
    }
}

async function getInstagramInfo(username) {
    const url = 'https://socialblade.com/instagram/user/'
    const browser = await puppeteer.launch({
        args: [...chrome.args, '--hide-scrollbars', '--disable-web-security'],
        defaultViewport: chrome.defaultViewport,
        executablePath: await chrome.executablePath,
        headless: true,
        ignoreHTTPSErrors: true,
    });
    const page = await browser.newPage();
    const headers = await page.setExtraHTTPHeaders({
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Mobile Safari/537.36',
        'cookie': '__auc=3540096117a51ccbc2bc5f32643; _ga=GA1.2.142326446.1624870010; _gid=GA1.2.1816228720.1624870010; _sp_v1_uid=1:787:f1e151c9-1c4a-4290-ac2a-5eb792933fb3; _sp_v1_csv=null; _sp_v1_lt=1:; consentUUID=fac0bfd4-56b2-4b1e-9dca-f199b4317d07; __qca=P0-782256388-1624870010380; _clck=3dax76; _fbp=fb.1.1624870011090.1981138731; euconsent-v2=CPIf3znPIf3znAGABCENBgCgAP_AAEdAAAqIGMwEwAFAANAArABgAGQAOAAgABaADSAHoAfABFACYAFsAQYAkABWgDkAH7AvIC8wGMgXlADABoAD4BeQBIyAEAEwBeYSAQABUADIAHAAQABFACYAP0BeYQACAvIVACACYAvMdAJAAqABkADgAIAAfABFACYAP0BeZCAEABkAJhKAIABkADgATAF5lIBAAFQAMgAcABAAEUAJgA_QF5lQAIAPgAAA.YAAAAAAAAAAA; _sp_v1_opt=1:login|true:last_id|11:; _pbjs_userid_consent_data=3524755945110770; _pubcid=ea49e632-eae7-414c-a165-14ab7a58eeb1; _lr_env_src_ats=false; pbjs-unifiedid={"TDID":"3c25811f-046a-429c-aa33-c5b52c47e364","TDID_LOOKUP":"TRUE","TDID_CREATED_AT":"2021-05-28T08:46:57"}; __gads=ID=a12ba912f21edc25:T=1624870016:S=ALNI_MaNImwwoF3TbFqKjsnjq9w8J0Aw-A; panoramaId_expiry=1624956417514; _sp_v1_ss=1:H4sIAAAAAAAAAItWqo5RKimOUbLKK83J0YlRSkVil4AlqmtrlXRIVRaNlZEHYhjUxuIygHQJbFbicyJug2IBnH4r6gsBAAA=; _sp_v1_consent=1!1:1:1:0:0:-1; _lr_geo_location=FR; PHPSESSXX=p5guog8jch38537u1c78eil6t1; __asc=ee9f7fdf17a52c643967b58aa94; _sp_v1_data=2:270492:1624870010:0:25:0:25:0:0:_:-1; cto_bidid=qSbTDV96T2xMdSUyQkR6TyUyQkxXJTJGbjhKS2o2cWRYMnltakJWN0trMUZkZGtFUzQlMkZLdE9lT1lLVTdWSHFpc0F6eSUyRmRuWGFqa1JyZTl6TiUyRkpGbEdhdW5HVDREUnlEbkVhRnpUcGpoNWJBZVRHbWZqdkNrbEliY3JyeUc5bFFraTN5VTRlbGZsRQ; cto_bundle=py22y19TcUdFVHZyTVRqMVdYSlVqRXZQUXluWGtPTE92cXFpS3E0MXQ5VEZNRnRpdFFyclJ4cFE5bmtzbUMlMkZISWVMRERwYUl2THp2QmxWWWJ5WUdPSjFnTmJHeTZieWVHY3pCdiUyRmRJUCUyRmFmU3NCbFU2c0tIQUkxekFzQmU4RDJ5S1JsTnNSM1lkald0QUljYVVTblhnNU5CUXclM0QlM0Q; cto_bundle=22O3WF9TcUdFVHZyTVRqMVdYSlVqRXZQUXlnNDEycHVsaVBXbmJPTHIydTZRQnltV3c2UE9MZFF4czA0cFFlbVhwdVlJdlhpNnFFTGRLeFBWSmNDaWkxTkE5R3JIZEhudlMlMkZVTlA0NkglMkJUOU9oSmpDN2NMbFhveGJEWDNvbmUzSU56MG9DSmZtTGxaM0hpcnF6bVBseHk5ejZRJTNEJTNE; _clsk=om26vc|1624889603377|13|1|eus/collect; lngtd-sdp=33'
    })
    await page.goto(url + username);
    const content = await page.content()
    const $ = cheerio.load(content);
    const test = $('.YouTubeUserTopInfo').toArray();
    const infos = [];
    test.forEach((item) => {
        item.children.forEach((child) => {
            if (typeof(child.attribs) !== 'undefined' && typeof(child.attribs.style) !== 'undefined' && child.name === 'span') {
                if (typeof(child.children[0].data) !== 'undefined' && Number.isNaN(child.children[0].data[0]) === false) {
                    const data = child.children[0].data.replace(/\s+/g, '').replace(',', '');
                    infos.push(parseFloat(data));
                }
            }
        })
    })
    const extracted = {
        username: username,
        uploads: infos[0],
        followers: infos[1],
        following: infos[2],
        engagement: infos[3],
        avgLikes: infos[4],
        avgComments: infos[5]
    }
    await browser.close();
    return extracted;
}

async function getTiktokInfo(username) {
    const url = 'https://socialblade.com/tiktok/user/'
    const browser = await puppeteer.launch();
    const page = await browser.newPage();
    const headers = await page.setExtraHTTPHeaders({
        'user-agent': 'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.106 Mobile Safari/537.36',
        'cookie': '__auc=3540096117a51ccbc2bc5f32643; _ga=GA1.2.142326446.1624870010; _gid=GA1.2.1816228720.1624870010; _sp_v1_uid=1:787:f1e151c9-1c4a-4290-ac2a-5eb792933fb3; _sp_v1_csv=null; _sp_v1_lt=1:; consentUUID=fac0bfd4-56b2-4b1e-9dca-f199b4317d07; __qca=P0-782256388-1624870010380; _clck=3dax76; _fbp=fb.1.1624870011090.1981138731; euconsent-v2=CPIf3znPIf3znAGABCENBgCgAP_AAEdAAAqIGMwEwAFAANAArABgAGQAOAAgABaADSAHoAfABFACYAFsAQYAkABWgDkAH7AvIC8wGMgXlADABoAD4BeQBIyAEAEwBeYSAQABUADIAHAAQABFACYAP0BeYQACAvIVACACYAvMdAJAAqABkADgAIAAfABFACYAP0BeZCAEABkAJhKAIABkADgATAF5lIBAAFQAMgAcABAAEUAJgA_QF5lQAIAPgAAA.YAAAAAAAAAAA; _sp_v1_opt=1:login|true:last_id|11:; _pbjs_userid_consent_data=3524755945110770; _pubcid=ea49e632-eae7-414c-a165-14ab7a58eeb1; _lr_env_src_ats=false; pbjs-unifiedid={"TDID":"3c25811f-046a-429c-aa33-c5b52c47e364","TDID_LOOKUP":"TRUE","TDID_CREATED_AT":"2021-05-28T08:46:57"}; __gads=ID=a12ba912f21edc25:T=1624870016:S=ALNI_MaNImwwoF3TbFqKjsnjq9w8J0Aw-A; panoramaId_expiry=1624956417514; _sp_v1_ss=1:H4sIAAAAAAAAAItWqo5RKimOUbLKK83J0YlRSkVil4AlqmtrlXRIVRaNlZEHYhjUxuIygHQJbFbicyJug2IBnH4r6gsBAAA=; _sp_v1_consent=1!1:1:1:0:0:-1; _lr_geo_location=FR; PHPSESSXX=p5guog8jch38537u1c78eil6t1; __asc=ee9f7fdf17a52c643967b58aa94; _sp_v1_data=2:270492:1624870010:0:25:0:25:0:0:_:-1; cto_bidid=qSbTDV96T2xMdSUyQkR6TyUyQkxXJTJGbjhKS2o2cWRYMnltakJWN0trMUZkZGtFUzQlMkZLdE9lT1lLVTdWSHFpc0F6eSUyRmRuWGFqa1JyZTl6TiUyRkpGbEdhdW5HVDREUnlEbkVhRnpUcGpoNWJBZVRHbWZqdkNrbEliY3JyeUc5bFFraTN5VTRlbGZsRQ; cto_bundle=py22y19TcUdFVHZyTVRqMVdYSlVqRXZQUXluWGtPTE92cXFpS3E0MXQ5VEZNRnRpdFFyclJ4cFE5bmtzbUMlMkZISWVMRERwYUl2THp2QmxWWWJ5WUdPSjFnTmJHeTZieWVHY3pCdiUyRmRJUCUyRmFmU3NCbFU2c0tIQUkxekFzQmU4RDJ5S1JsTnNSM1lkald0QUljYVVTblhnNU5CUXclM0QlM0Q; cto_bundle=22O3WF9TcUdFVHZyTVRqMVdYSlVqRXZQUXlnNDEycHVsaVBXbmJPTHIydTZRQnltV3c2UE9MZFF4czA0cFFlbVhwdVlJdlhpNnFFTGRLeFBWSmNDaWkxTkE5R3JIZEhudlMlMkZVTlA0NkglMkJUOU9oSmpDN2NMbFhveGJEWDNvbmUzSU56MG9DSmZtTGxaM0hpcnF6bVBseHk5ejZRJTNEJTNE; _clsk=om26vc|1624889603377|13|1|eus/collect; lngtd-sdp=33'
    })
    await page.goto(url + username);
    const content = await page.content()
    const $ = cheerio.load(content);
    const test = $('.YouTubeUserTopInfo').toArray();
    const infos = [];
    test.forEach((item) => {
        item.children.forEach((child) => {
            if (typeof(child.attribs) !== 'undefined' && typeof(child.attribs.style) !== 'undefined' && child.name === 'span') {
                if (typeof(child.children[0].data) !== 'undefined' && Number.isNaN(child.children[0].data[0]) === false) {
                    const data = child.children[0].data.replace(/\s+/g, '').replace(/\,/g, '');
                    infos.push(parseFloat(data));
                }
            }
        })
    })
    const extracted = {
        username: username,
        uploads: infos[0],
        followers: infos[1],
        following: infos[2],
        likes: infos[3],
    }
    await browser.close();
    return extracted;
}

export default async function handler(req, res) {
    if (req.method === 'GET') {
        let result = 'Not found';
        if (typeof(req.query.username) !== 'undefined' && typeof(req.query.platform) !== 'undefined') {
            const username = req.query.username;
            const platform = req.query.platform;
            console.log(req.query)
            if (platform === 'instagram') {
                result = await getInstagramInfo(username);
                console.log(result)
            }
            if (platform === 'youtube') {
                result = await getYoutubeInfo(username);
                console.log(result)
            }
        }
        res.status(200).json(result)
    }
    else {
        res.status(405).end()
    }
}