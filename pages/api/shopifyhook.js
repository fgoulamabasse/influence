import { supabase } from '../../lib/initSupabase'
import moment from 'moment'

async function findInfluencer(coupon, landing) {
    if (coupon) {
        const { data: influencerDetail, error } = await supabase.from('influencerDetails').select('influencer').eq('coupon', coupon).single()
        return influencerDetail ? influencerDetail.influencer : null
    }
    else {
        let index = landing.search("utm_source")
        if (index === -1) {
            return null;
        }
        else {
            let source = landing.substr(index);
            source = source.substr(source.indexOf('=') + 1);
            const { data: influencerDetail, error } = await supabase.from('influencerDetails').select('influencer').eq('username', source).single()
            return influencerDetail ? influencerDetail.influencer : null
        }
    }
}

async function findPost(coupon, landing, created_at) {
    const influencerId = findInfluencer(coupon, landing)
    if (influencerId) {
        const { data: posts, error } = await supabase.from('posts').select('id, date, influencer').order('date', { ascending: false }).eq('influencer', influencerId)
        if (posts) {
            const postsBefore = posts.filter(item => item.influencer && item.influencer === influencerId && moment(item.date).isBefore(moment(created_at)));
            const result = postsBefore.length > 0 ? postsBefore[0] : null;
            return result;
        }
    }
    return null;
}

export default async function handler(req, res) {
    if (req.method === 'POST') {
        console.log(req.body)
        const data = req.body
        const coupon = data.discount_codes.length > 0 ? data.discount_codes[0].code : null
        const post = await findPost(coupon, data.landing_site, data.created_at)
        const postId = post ? post.id : null;
        const order = {
            date: data.created_at,
            email: data.email,
            landing: data.landing_site,
            price: data.current_total_price,
            discount: data.total_discounts,
            coupon: coupon,
            newClient: data.customer.orders_count > 1 ? false : true,
            post: postId,
            businessUnit: 1
        }
        const { data: insert, error } = await supabase.from('attribution').insert(order);
        console.log(insert)
        res.status(200).end()
    }
    else {
        res.status(405).end()
    }
}