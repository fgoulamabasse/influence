import { Auth } from '@supabase/ui'
import { supabase } from '../lib/initSupabase'
import '../styles/index.css';
import 'react-toastify/dist/ReactToastify.css';
import 'rsuite/dist/styles/rsuite-default.css';

export default function MyApp({ Component, pageProps }) {
  return (
    <Auth.UserContextProvider supabaseClient={supabase}>
      <Component {...pageProps} />
    </Auth.UserContextProvider>
  )
}
